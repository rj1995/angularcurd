import { Component, OnInit } from '@angular/core';
import { ProdductListService } from '../prodduct-list.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  product: any;
model={
  "id": "",
  "Product": "",
  "Quntity": "",
  "discription": ""
}
  constructor(private ProdductListService:ProdductListService) { }

  ngOnInit() {
    // this.ProdductListService.getProduct()
    // .subscribe((res)=>{
    //   this.product=res;
    // })
    this.getProduct();
    
  }
  getProduct()
  {
    this.ProdductListService.getProduct()
    .subscribe((res)=>{
      this.product=res;
    })
  }
  editEmp(data){
console.log(data)
this.model = data;

  }
  onSubmit(f) {
 
    console.log('f');  
    this.ProdductListService.PutProduct(this.model)  
      .subscribe(() => {
        alert("Product Updated!"); 
        this.model={
          "id": "",
          "Product": "",
          "Quntity": "",
          "discription": ""
        }
        this.getProduct();
        },  
      error => {  
        alert("error");  
      });  
     
}
deleteEmp(data){
  console.log('f');  
  this.ProdductListService.DeleteProduct(data.id)  
    .subscribe(() => {
      alert("Product Deleted!"); 
     
      this.getProduct();
      },  
    error => {  
      alert("error");  
    });  
}
}
