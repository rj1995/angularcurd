import { TestBed } from '@angular/core/testing';

import { ProductSaleService } from './product-sale.service';

describe('ProductSaleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductSaleService = TestBed.get(ProductSaleService);
    expect(service).toBeTruthy();
  });
});
