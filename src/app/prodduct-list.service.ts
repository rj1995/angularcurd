import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ProdductListService {

  constructor(private http:HttpClient ) { }
  getProduct()
    {
    return this.http.get('https://5c78f6693a89af0014cd7157.mockapi.io/api/data');
    }
    
  postProduct(data)
  {
    return this.http.post('https://5c78f6693a89af0014cd7157.mockapi.io/api/data',data);
  }

  PutProduct(data)
  {
    return this.http.put('https://5c78f6693a89af0014cd7157.mockapi.io/api/data/'+data.id,data);
  }
  DeleteProduct(id)
  {
    debugger
    return this.http.delete('https://5c78f6693a89af0014cd7157.mockapi.io/api/data/'+ id) ;
  }
}


