import { TestBed } from '@angular/core/testing';

import { ProdductListService } from './prodduct-list.service';

describe('ProdductListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProdductListService = TestBed.get(ProdductListService);
    expect(service).toBeTruthy();
  });
});
