import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DashbordComponent } from './dashbord/dashbord.component';
import { ProductComponent } from './product/product.component';
import { SaleComponent } from './sale/sale.component';
import { AboutComponent } from './about/about.component';
import {RouterModule} from '@angular/router';
import { AddFormComponent } from './add-form/add-form.component';
import { FormsModule } from '@angular/forms';
import { ProdductListService } from './prodduct-list.service';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashbordComponent,
    ProductComponent,
    SaleComponent,
    AboutComponent,
    AddFormComponent,
   
  ],
  imports: [
    BrowserModule,HttpClientModule, BrowserModule,
    FormsModule,
    RouterModule.forRoot([
     {path : '',component :DashbordComponent},
     {path:'dashboard' ,component:DashbordComponent},
     {path:'product' ,component:ProductComponent},
     {path:'add-form' ,component:AddFormComponent}
     
    ])
  ],
  providers: [ProdductListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
