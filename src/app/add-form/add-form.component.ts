import { Component, OnInit } from '@angular/core';
import { ProdductListService } from '../prodduct-list.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {
  
  product: any;
  constructor(private productlistservice:ProdductListService,
    private router:Router) { }

  ngOnInit() {
    
  }

  onSubmit(f) {
 
      console.log('f');  
      this.productlistservice.postProduct(f.value)  
        .subscribe(() => {
            this.router.navigate(['/product']);
          },  
        error => {  
          alert(error);  
        });  
       
  }
  
}
